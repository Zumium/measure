/*
本模块的入口文件，首先装载本模块的配置，
然后启动HTTP服务，待所有启动过程完毕后向主模块汇报启动完成
*/
const Promise=require('bluebird');
const express=require('express');
const fs=Promise.promisifyAll(require('fs'));
const config=require('./config/config');
const errHandler=require('./components/error-handler');
const router=require('./components/router');
const sendready=require('./config/sendready');

const app=express();

app.use(router);
app.use(errHandler);

Promise.resolve()
.then(
	()=>config.load()
)
.then(
	()=>fs.accessAsync(config.get('serverSocket'),fs.constants.F_OK)
)
.then(
	()=>fs.unlinkAsync(config.get('serverSocket')),
	err=>Promise.resolve()
)
.then(
	()=>app.listen(config.get('serverSocket'))
)
.then(
	()=>sendready('Measure')
);

//Does nothing when receiving SIGQUIT
process.on('SIGQUIT',()=>'');
