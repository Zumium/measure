#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct measure_data_struct {
	char addr[41];
	double latency;
	int drop;
	struct measure_data_struct* next;
} measure_data;

typedef struct measure_data_header_struct {
	int len;
	measure_data* head;
} measure_data_header;

void add_tail(measure_data_header*,measure_data*);
measure_data* get_head(measure_data_header*);
measure_data* get_next(measure_data*);
measure_data* find_by_addr(measure_data_header*,char*);
measure_data* new_data(char*);
measure_data_header* new_list();
void free_list(measure_data_header*);

#endif
