#include <string.h>
#include <stdlib.h>
#include "linkedlist.h"

void add_tail(measure_data_header* list,measure_data* data){
	if(list->len==0){
		list->head=data;
		list->len++;
		return;
	}
	measure_data* ptr=list->head;
	for(int i=1;i<list->len;i++)
		ptr=ptr->next;
	ptr->next=data;
	list->len++;
	return;
}

measure_data* get_head(measure_data_header* list){
	return list->head;
}

measure_data* get_next(measure_data* data){
	return data->next;
}

measure_data* find_by_addr(measure_data_header* list,char* addr){
	if(list->len==0)
		return NULL;
	measure_data* ptr;
	for(ptr=list->head;ptr!=NULL;ptr=ptr->next)
		if(strcmp(ptr->addr,addr)==0)
			return ptr;
	return NULL;
}

measure_data* new_data(char* addr){
	size_t measure_data_size=sizeof(measure_data);
	measure_data* data=(measure_data*)malloc(measure_data_size);
	memset(data,0,measure_data_size);
	strncpy(data->addr,addr,41);
	return data;
}

measure_data_header* new_list(){
	size_t measure_data_header_size=sizeof(measure_data_header);
	measure_data_header* list=(measure_data_header*)malloc(measure_data_header_size);
	memset(list,0,measure_data_header_size);
	return list;
}

void free_list(measure_data_header* list){
	if(list->len!=0)
		for(measure_data* ptr=list->head;ptr!=NULL;){
			measure_data* nxt=ptr->next;
			free(ptr);
			ptr=nxt;
		}
	free(list);
}
