#include <json-c/json.h>
#include <oping.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <oping.h>
#include "linkedlist.h"

int print_error(char*);
double get_latency(pingobj_iter_t*);
void get_addr(pingobj_iter_t*,char*,int);
int get_drop(pingobj_iter_t*);

int main(int argc,char* argv[]){
	/*
	 * 网络质量动态监测程序
	 * C语言部分
	 *
	 * 传入参数为JSON字符串
	 */

	/*
	 * 1. 检查参数个数
	 */
	if(argc!=2)	//FOR PRODUCTION
		return print_error("Wrong argument number");

	/*
	 * 2. 解析输入JSON
	 */
	json_tokener* parse_token=json_tokener_new();
	json_object* arg_json=json_tokener_parse_ex(parse_token,argv[argc-1],strlen(argv[argc-1]));
	if(json_tokener_get_error(parse_token)!=json_tokener_success){
		json_tokener_free(parse_token);
		return print_error("Argument parsing failed");
	}
	json_tokener_free(parse_token);

	/*
	 * 3. 读取所需的参数
	 */

	int packet_num=0;
	int addresses_num=0;
	json_object* addresses_arr=json_object_new_object();
	double timeout=0;

	pingobj_t* pinger=ping_construct(); //Specially put here
	measure_data_header* list=new_list();

	// 3.1 获取packetNum
	json_object* packet_num_json=json_object_new_object();
	if(!json_object_object_get_ex(arg_json,"packetNum",&packet_num_json))
		return print_error("Cannot read property packetNum");
	packet_num=(int)json_object_get_int(packet_num_json);
	json_object_put(packet_num_json);

	// 3.2 获取addresses
	if(!json_object_object_get_ex(arg_json,"addresses",&addresses_arr))
		return print_error("Cannot read property addresses");
	addresses_num=json_object_array_length(addresses_arr);

	for(int idx=0;idx<addresses_num;idx++){
		const char* addr=json_object_get_string(json_object_array_get_idx(addresses_arr,idx));
		//for each address
		ping_host_add(pinger,addr);
		add_tail(list,new_data((char*)addr));
	}

	// 3.3 获取timeout
	json_object* timeout_json=json_object_new_object();
	if(!json_object_object_get_ex(arg_json,"timeout",&timeout_json)){
		return print_error("Cannot read property timeout");
	}
	timeout=json_object_get_double(timeout_json);
	json_object_put(timeout_json);

	/*
	 * 5 开始测量
	 */

	ping_setopt(pinger,PING_OPT_TIMEOUT,&timeout);
	for(int times=0;times<packet_num;times++){
		ping_send(pinger);
		// 5.1 读取延迟数据
		for(pingobj_iter_t* iter=ping_iterator_get(pinger);iter != NULL;iter=ping_iterator_next(iter)){
			char addr[50]={0};
			get_addr(iter,addr,50);

			find_by_addr(list,addr)->latency+=get_latency(iter);
		}
	}
	// 5.2 读取丢包数据
	for(pingobj_iter_t* iter=ping_iterator_get(pinger);iter!=NULL;iter=ping_iterator_next(iter)){
		char addr[50]={0};
		get_addr(iter,addr,50);

		find_by_addr(list,addr)->drop=get_drop(iter);
	}
	// 5.3 装填返回JSON
	json_object* ret_json=json_object_new_object();
	for(measure_data* ptr=get_head(list);ptr!=NULL;ptr=get_next(ptr)){
		json_object* ret_json_per_addr=json_object_new_object();
		json_object_object_add(ret_json_per_addr,"latency",json_object_new_double(ptr->latency/packet_num));
		json_object_object_add(ret_json_per_addr,"pktloss",json_object_new_double((ptr->drop)*1.0/packet_num));
		
		json_object_object_add(ret_json,ptr->addr,ret_json_per_addr);
	}

	// 5.4 输出结果
	fprintf(stdout,"%s\n",json_object_to_json_string(ret_json));

	json_object_put(ret_json);
	ping_destroy(pinger);
	free_list(list);

	return 0;
}

int print_error(char* err_msg_str){
		json_object* err_obj=json_object_new_object();
		json_object* err_msg=json_object_new_string(err_msg_str);
		json_object_object_add(err_obj,"message",err_msg);
		fprintf(stderr,"%s\n",json_object_to_json_string(err_obj));
		json_object_put(err_msg);
		json_object_put(err_obj);
		return -1;
}

double get_latency(pingobj_iter_t* obj){
	double latency=0;
	size_t buffer_size=sizeof(latency);
	ping_iterator_get_info(obj,PING_INFO_LATENCY,&latency,&buffer_size);
	return latency;
}

void get_addr(pingobj_iter_t* obj,char* buf,int buf_size){
	size_t buffer_size=(size_t)buf_size;
	ping_iterator_get_info(obj,PING_INFO_USERNAME,buf,&buffer_size);
}

int get_drop(pingobj_iter_t* obj){
	int drop=0;
	size_t buf_size=sizeof(drop);
	ping_iterator_get_info(obj,PING_INFO_DROPPED,&drop,&buf_size);
	return drop;
}
