const Promise=require('bluebird');
const express=require('express');
const bodyParser=require('body-parser');
const execFile=Promise.promisify(require('child_process').execFile);
const _=require('underscore');
const genError=require('./gene-error');
const config=require('../config/config');

var router=module.exports=express.Router();
router.use(bodyParser.json());

/*
 * 执行实际的测量
 * 需要测量的节点的地址通过POST请求以JSON形式传过来，模块解析JSON数据获取测量节点列表
 * 然后以子进程的方式启动测量过程，将相关参数以JSON字符串形式传入子进程，接收子进程以标准输出方式传出的测量结果
 * 测量结果也是JSON字符串，解析字符串后获取其中的结果
 * 打包好后放入POST响应中回复
 */
router.post('/measure',
	(req,res,next)=>{
		req.measureDefaults={
			packetNum: config.get('packetNum'),
			timeout: config.get('timeout')
		};
		next();
	},
	(req,res,next)=>
	/*
	 * 请求格式
	 * {
	 *	addresses:['IP地址', ... ]
	 * }
	 *
	 */
		execFile(config.get('measureProg'),[JSON.stringify(_.defaults(req.body,req.measureDefaults))])
		.then(stdout=>
			res.json(
				_.values(
					_.mapObject(JSON.parse(stdout),(val,key)=>{
						val['address']=key;
						return val;
					})))
		)
		.catch(next)
);

router.delete('/',(req,res)=>{
	res.sendStatus(200);
	process.exit();
});
