const request = require('request');
const Promise = require('bluebird');

module.exports=moduleName=>new Promise(
  (resolve,reject)=>
    request
    .post(
      {
        uri: getUri(process.argv[process.argv.length-1],'/moduleready'),
        json: true,
        body: {moduleName: moduleName}
      },
      (error,response,body)=>{
        if(error)
          return reject(error);
        if(response.statusCode !== 200)
          return reject(new Error('Announcing Ready Error:'+response.statusCode+'   '+response.statusMessage));
        resolve();
      }
    )
);

const getUri=(socketPath,path)=>'http://unix:'+socketPath+':'+path;
